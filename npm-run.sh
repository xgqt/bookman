#!/bin/sh


# This file is part of bookman.

# bookman is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.

# bookman is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with bookman.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (c) 2021, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v3 License
# SPDX-License-Identifier: GPL-3.0-only


trap 'exit 128' INT
set -e
export PATH


cd ./src/browser

! [ -d ./node_modules/.bin ] && npm install --local

PATH="${PATH}:$(pwd)/node_modules/.bin"
export PATH


if [ -z "${1}" ]
then
    npm run
else
    for cmd in "${@}"
    do
        npm run "${cmd}"
    done
fi
