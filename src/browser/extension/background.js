// This file is part of bookman.

// bookman is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3.

// bookman is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with bookman.  If not, see <https://www.gnu.org/licenses/>.

// Copyright (c) 2021, Maciej Barć <xgqt@riseup.net>
// Licensed under the GNU GPL v3 License
// SPDX-License-Identifier: GPL-3.0-only


if ( typeof browser === "undefined" ) {
    var browser = chrome;
}


var port = browser.runtime.connectNative("com.xgqt.bookman_browser");

// Listen for messages from the app
port.onMessage.addListener(
    (response) => {
        console.log("Received: " + response.active);
    });

// On a click on the browser action, send the app a message
// browser.browserAction.onClicked.addListener(
//     () => {
//         console.log("Sending:  ping");
//         port.postMessage("ping");
//     });
