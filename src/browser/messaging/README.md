# Paths


## Linux

Chromium: `~/.config/chromium/NativeMessagingHosts/com.xgqt.bookman-browser.json`

Firefox: `~/.mozilla/native-messaging-hosts/com.xgqt.bookman-browser.json`


# Documentation


## Chrome

https://developer.chrome.com/docs/apps/nativeMessaging/


## Firefox

https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Native_manifests#manifest_location
